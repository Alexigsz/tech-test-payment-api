using System.ComponentModel.DataAnnotations;

namespace PaymentAPI.Models
{
    public enum EnumStatusVenda
    {
        [Display(Name="Aguardando Pagamento")]
        AguardandoPagamento,
        [Display(Name="Pagamento Aprovado")]
        PagamentoAprovado,
        [Display(Name="Enviado para transportadora")]
        EnviadoParaTransportadora,
        [Display(Name="Entregue")]
        Entregue,
        [Display(Name="Cancelada")]
        Cancelada
    }
}