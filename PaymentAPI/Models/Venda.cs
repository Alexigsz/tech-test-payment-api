using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentAPI.Models
{
    public class Venda
    {
        public int Id { get; set; }
        public virtual Vendedor Vendedor { get; set; }
        public virtual List<Item> Itens { get; set; }
        public DateTime Data { get; set; }
        public EnumStatusVenda Status { get; set; }
    }
}