using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentAPI.Models
{
    public class Item
    {
        public int Id { get; set; }
        public virtual Produto Produto { get; set; }
        public int Quantidade { get; set; }
    }
}