using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PaymentAPI.Models;

namespace PaymentAPI.Helpers
{
    public static class PaymentHelper
    {
        public static bool ValidarTransicaoStatus(EnumStatusVenda statusAntigo, EnumStatusVenda statusNovo)
        {
            if (statusAntigo == EnumStatusVenda.AguardandoPagamento 
                && (statusNovo == EnumStatusVenda.PagamentoAprovado || statusNovo == EnumStatusVenda.Cancelada))
                return true;

            if (statusAntigo == EnumStatusVenda.PagamentoAprovado
                && (statusNovo == EnumStatusVenda.EnviadoParaTransportadora || statusNovo == EnumStatusVenda.Cancelada))
                return true;

            if (statusAntigo == EnumStatusVenda.EnviadoParaTransportadora && statusNovo == EnumStatusVenda.Entregue)
                return true;

            return false;
        }
    }
}