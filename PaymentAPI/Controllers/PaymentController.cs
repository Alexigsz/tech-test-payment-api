using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PaymentAPI.Context;
using PaymentAPI.Models;
using PaymentAPI.Helpers;

namespace PaymentAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PaymentController : ControllerBase
    {
        private readonly PaymentContext _context;

        public PaymentController(PaymentContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult RegistrarVenda(Venda venda)
        {
            venda.Status = EnumStatusVenda.AguardandoPagamento;

            if (venda.Itens.Count == 0)
                return BadRequest("Venda não pode ser cadastrada sem itens");

            _context.Add(venda);
            _context.SaveChanges();
            return Ok();
        }

        [HttpGet]
        public IActionResult Listar()
        {
            var vendas = _context.Vendas.ToList();
            return Ok(vendas);
        }

        [HttpGet("{id}")]
        public IActionResult BuscarVenda(int id)
        {
            var venda = _context.Vendas.Find(id);

            if (venda == null)
                return NotFound();

            return Ok(venda);
        }

        [HttpPut("{id}")]
        public IActionResult AtualizarVenda(int id, Venda venda)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if (venda == null)
                return NotFound();

            if (!PaymentHelper.ValidarTransicaoStatus(vendaBanco.Status, venda.Status))
                return BadRequest("Transição de Status inválida");

            vendaBanco.Status = venda.Status;

            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();

            return Ok(venda);
        }
    }
}