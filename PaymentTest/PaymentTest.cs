using PaymentAPI.Models;
using PaymentAPI.Helpers;

namespace PaymentTest;

public class PaymentTest
{
    [Theory]
    [InlineData(EnumStatusVenda.AguardandoPagamento, EnumStatusVenda.PagamentoAprovado)]
    [InlineData(EnumStatusVenda.AguardandoPagamento, EnumStatusVenda.Cancelada)]
    [InlineData(EnumStatusVenda.PagamentoAprovado, EnumStatusVenda.EnviadoParaTransportadora)]
    [InlineData(EnumStatusVenda.PagamentoAprovado, EnumStatusVenda.Cancelada)]
    [InlineData(EnumStatusVenda.EnviadoParaTransportadora, EnumStatusVenda.Entregue)]
    public void DeveAtualizarStatus_RetornarTrue(EnumStatusVenda statusAntigo, EnumStatusVenda statusNovo)
    {
        var resultado = PaymentHelper.ValidarTransicaoStatus(statusAntigo, statusNovo);

        Assert.True(resultado);
    }
}